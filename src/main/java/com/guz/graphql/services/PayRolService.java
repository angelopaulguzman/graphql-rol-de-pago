package com.guz.graphql.services;

import com.guz.graphql.data.PayRolDocument;
import com.guz.graphql.data.PayRolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PayRolService {
    private PayRolRepository payRolRepository;

    @Autowired
    public PayRolService(PayRolRepository payRolRepository) {
        this.payRolRepository = payRolRepository;
    }

    public List<PayRolDocument> getAllPays(){
        return this.payRolRepository.findAll();
    }


    public PayRolDocument getPayById(String id){
        Optional<PayRolDocument> PayDocumentOptional = this.payRolRepository.findById(id);
        
        return PayDocumentOptional.get();
    }


}
